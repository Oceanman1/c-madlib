#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

std::string  MadLib(std::string lib_filler[], std::string lib[], int num_lib)
{
	std::string libString;
	for (int i = 0; i < num_lib; i++)
	{
		std::cout << lib_filler[i];
		std::cout << lib[i];
	}

	std::cout << " it over.\"";
	
	return libString;
}

int main()
{
	char input;
	std::string output;
	int const num_lib = 17;
	std::string lib[num_lib];
	std::string lib_type[num_lib] = { "a plural noun", "an adverb", "a verb", "an article of clothing", "a body part", "an adjective",
	"a noun", "a plural noun", "another body part", "a plural noun", "another body part", "a noun", "a noun", "a verb ending in \"ing\"",
	"an adjective", "an adjective", "a verb" };
	std::string lib_filler[num_lib] = { "\nIt's simple.  Turn the ", ". Make him/her want ", " to date you.  Make sure they're always dressed to ",
	".  Each and every day wear a/an ", " that you know shows off your ", " to ", " advantage and make your ", " look like a million ",
	".  Even if the two of you make meaningful ", " contact, don't admit it.  No hugs or ", ". Just shake his/her ",
	" firmly.  And remember, when he/she asks you out, even though a chill may run down your ", " and you can't stop your ", " from ",
	", just play it ", ".  Take a long pause before answering in a very ", " voice.  \"I'll have to "};

	for (int i = 0; i < num_lib; i++)
	{
		std::cout << "Enter " << lib_type[i] << " : ";
		std::cin >> lib[i];
	}
	
	output = MadLib(lib_filler, lib, num_lib);

	std::cout << "\nWould you like to save your results in a text file?  Press y for yes and any other key to exit.";
	std::cin >> input;

	if (input == 'y' || input == 'Y')
	{
		std::string path = "C:\\c_text\\test.txt";

		std::ofstream ofs(path);
		for (int i = 0; i < num_lib; i++)
		{
			ofs << lib_filler[i];
			ofs << lib[i];
		}
		ofs << " it over.\"";
		ofs.close();
	}


	_getch();
	return 0;
}

